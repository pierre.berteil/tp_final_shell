#!/bin/bash

apt-get install apache2
echo 'installation de apache.'
apt-get install php-mcrypt
apt-get install curl
apt install php7.2-cli
echo 'installation de php'
curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer
apt-get install composer
echo 'installation de composer'
apt-get install mysql-server
echo 'installation de mysql'
echo "nom d'utilisateur pour mysql:"
read dbname
echo "mot de passe pour mysql:"
read dbpassword
mysql -u root -p -e "CREATE USER '$dbname'@'localhost' IDENTIFIED BY '$password';"
echo "nom de la base de donée:"
read nombdd
mysql -u root -p -e "CREATE DATABASE $nombdd"
echo "lien du git"
read liengit
apt install git
cd /var/www/html/
git clone $liengit
cd $(ls | sed -n 2p)
composer install
php artisan migrate
php artisan db:seed
